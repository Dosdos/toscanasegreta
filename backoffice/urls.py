from django.conf.urls import patterns


urlpatterns = patterns(
    'backoffice.views',

    # Home
    (r'^[/]?$', 'home', {'template_name': 'backoffice/home.html'}, 'backoffice_home'),
    (r'^carica_csv[/]?$', 'load_by_csv', {'template_name': 'backoffice/load_by_csv.html'}, 'backoffice_load_by_csv'),

)
