from backoffice.models import POI
from django.contrib import admin

# Register your models here.
admin.site.register(POI)
