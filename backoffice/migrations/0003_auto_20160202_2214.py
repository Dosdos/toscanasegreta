# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('backoffice', '0002_auto_20160131_0053'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='poi',
            name='categoria_prevalente',
        ),
        migrations.RemoveField(
            model_name='poi',
            name='numero_sedi',
        ),
    ]
