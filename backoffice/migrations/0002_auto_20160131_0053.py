# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('backoffice', '0001_initial'),
    ]

    operations = [
        migrations.AddField(
            model_name='poi',
            name='categoria_prevalente',
            field=models.CharField(max_length=100, null=True, blank=True),
        ),
        migrations.AddField(
            model_name='poi',
            name='comune',
            field=models.CharField(max_length=100, null=True, blank=True),
        ),
        migrations.AddField(
            model_name='poi',
            name='indirizzo',
            field=models.CharField(max_length=100, null=True, blank=True),
        ),
        migrations.AddField(
            model_name='poi',
            name='numero_sedi',
            field=models.IntegerField(default=1, blank=True),
        ),
        migrations.AddField(
            model_name='poi',
            name='provincia',
            field=models.CharField(max_length=100, null=True, blank=True),
        ),
        migrations.AddField(
            model_name='poi',
            name='tipologia',
            field=models.CharField(max_length=500, null=True, blank=True),
        ),
    ]
