from backoffice.models import POI
from django import forms


class POIForm(forms.ModelForm):
    class Meta:
        model = POI
        fields = (
            'name',
            'lat',
            'lon',
            'desc',
        )

        widgets = {
            'name': forms.TextInput(attrs={'type': "text", 'class': "form-control", 'placeholder': "Name"}),
            'lat': forms.TextInput(attrs={'type': "text", 'class': "form-control", 'placeholder': "Lat"}),
            'lon': forms.TextInput(attrs={'type': "text", 'class': "form-control", 'placeholder': "Lon"}),
            'desc': forms.TextInput(attrs={'type': "text", 'class': "form-control", 'placeholder': "Desc"}),
        }


class ImportPOIForm(forms.Form):
    file_csv = forms.FileField(required=True)
