# -*- coding: utf-8 -*-

from backoffice.forms import POIForm, ImportPOIForm
from backoffice.models import POI
from django.contrib.auth.decorators import login_required, user_passes_test
from django.core.urlresolvers import reverse
from django.db import transaction
from django.http import HttpResponseRedirect
from django.shortcuts import render_to_response
from django.template import RequestContext


# ===========================================================================
# USER PASSES TESTS
# ===========================================================================
def is_admin(user):
    return user.is_staff


# ===========================================================================
# HOME
# ===========================================================================
@login_required(login_url='/login/')
@user_passes_test(is_admin)
def home(request, template_name="backoffice/home.html"):

    # Create unbound forms
    form = POIForm()

    if request.method == 'POST':
        with transaction.atomic():
            # Create bound forms
            form = POIForm(request.POST)
            if form.is_valid():
                # Get user from POST and save it
                form.save()
                return HttpResponseRedirect(reverse('backoffice_home'))

    return render_to_response(template_name, {
        'session': request.session.keys(),
        'form': form,
        'POIs': POI.objects.all()
    }, context_instance=RequestContext(request))


@login_required(login_url='/login/')
def load_by_csv(request, template_name="reserved/course/course_load_by_csv.html"):
    import_form = ImportPOIForm()
    error = success = None

    if request.method == 'POST':
        import_form = ImportPOIForm(request.POST, request.FILES)
        if import_form.is_valid():

            # try:
            import csv
            file_in = request.FILES['file_csv']
            reader = [row for row in csv.reader(file_in)]

            # Init counters
            created = 0
            updated = 0

            # Iterate starting from second row
            for row in reader[1:]:

                try:
                    lat = row[0] if row[0] else 0.
                    lon = row[1] if row[1] else 0.
                    categoria = row[2] if row[2] else None
                    tipologia = row[3].strip() if row[3] else None
                    name = row[4].strip().lower() if row[4] else None
                    indirizzo = row[5].strip() if row[5] else None
                    comune = row[6].strip() if row[6] else None
                    provincia = row[7].strip() if row[7] else None

                    # check if a course with this name already exists:
                    # if it already exists, update info, add it otherwise
                    try:
                        poi = POI.objects.get(name=name)
                        poi.lat = lat if lat and lat != "" else 0.
                        poi.lon = lon if lon and lon != "" else 0.
                        poi.categoria = categoria if categoria else "nd"
                        poi.tipologia = tipologia if tipologia else "nd"
                        poi.name = name if name else "nd"
                        poi.indirizzo = indirizzo if indirizzo else "nd"
                        poi.comune = comune if comune else "nd"
                        poi.provincia = provincia if provincia else "nd"

                        updated += 1
                    except POI.DoesNotExist:
                        poi = POI(
                            lat=lat,
                            lon=lon,
                            categoria=categoria,
                            tipologia=tipologia,
                            name=name,
                            indirizzo=indirizzo,
                            comune=comune,
                            provincia=provincia,
                        )

                        created += 1
                    poi.save()
                except:
                    error = "Errore durante il caricamento del file. Verifica che il file sia corretto e prova di nuovo."
            file_in.close()
            success = "Caricamento terminato con successo. %d elementi aggiornati e %d nuovi elementi aggiunti." % (updated, created)

    return render_to_response(template_name, {
        'session': request.session.keys(),
        'success': success,
        'error': error,
        'import_form': import_form,
    }, context_instance=RequestContext(request))
