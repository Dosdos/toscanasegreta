from django.db import models

# Create your models here.
class POI(models.Model):
    name = models.CharField(max_length=100, null=True, blank=True)
    lat = models.DecimalField(max_digits=9, decimal_places=6)
    lon = models.DecimalField(max_digits=9, decimal_places=6)
    desc = models.CharField(max_length=500, null=True, blank=True)
    tipologia = models.CharField(max_length=500, null=True, blank=True)
    indirizzo = models.CharField(max_length=100, null=True, blank=True)
    comune = models.CharField(max_length=100, null=True, blank=True)
    provincia = models.CharField(max_length=100, null=True, blank=True)
    categoria = models.CharField(max_length=100, null=True, blank=True)
    last_update = models.DateTimeField(auto_now=True)

    class Meta:
        verbose_name = 'Point of Interest'
        verbose_name_plural = 'Points of Interest'

    def __unicode__(self):
        return self.name
