import urllib2
from backoffice.models import POI

import xmltodict as xmltodict
from django.shortcuts import render_to_response
from django.template import RequestContext

API_URL = "http://www501test.regione.toscana.it/SIM/oggetto/cerca?lat=%s&lon=%s&buffer=%s"


# ===========================================================================
# HOME
# ===========================================================================
def home(request, template_name="frontoffice/home.html"):
    return render_to_response(template_name, {
        'session': request.session.keys(),
    }, context_instance=RequestContext(request))


def map(request, template_name="frontoffice/map.html"):

    lat = 43.801893
    lon = 11.201623
    buff = 500

    # api_url = API_URL % (lat, lon, buff)
    # xml_result = urllib2.urlopen(api_url).read()
    # dict_result = dict(xmltodict.parse(xml_result)['note'])
    # dict_result = dict(xmltodict.parse(xml_result))

    return render_to_response(template_name, {
        'session': request.session.keys(),
        'lat': lat,
        'lon': lon,
        'buff': buff,
        # 'result': dict_result,
        'POIs': POI.objects.all()
    }, context_instance=RequestContext(request))


def app(request, template_name="frontoffice/app.html"):

    lat = 43.7776134
    lon = 11.2462708
    buff = 500

    return render_to_response(template_name, {
        'session': request.session.keys(),
        'lat': lat,
        'lon': lon,
        'POIs': POI.objects.all()[:500]
    }, context_instance=RequestContext(request))


def find_events(request, lat, lon, buff, template_name="frontoffice/home.html"):
    api_url = API_URL % (lat, lon, buff)
    result = urllib2.urlopen(api_url).read()

    return render_to_response(template_name, {
        'session': request.session.keys(),
        'result': result,
        'lat': lat,
        'lon': lon,
        'buff': buff,
    }, context_instance=RequestContext(request))
