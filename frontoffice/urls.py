from django.conf.urls import patterns


urlpatterns = patterns(
    'frontoffice.views',

    # Home
    (r'^[/]?$', 'home', {'template_name': 'frontoffice/home.html'}, 'frontoffice_home'),
    (r'^map[/]?$', 'map', {'template_name': 'frontoffice/map.html'}, 'frontoffice_map'),
    (r'^app[/]?$', 'app', {'template_name': 'frontoffice/app.html'}, 'frontoffice_app'),
    (r'^find_events/(?P<lat>[-\w]+)/(?P<lon>[-\w]+)/(?P<buffer>[-\w]+)[/]?$', 'find_events', {'template_name': 'frontoffice/home.html'}, 'find_events'),

)


#########################################
# Django REST framework                 #
# http://www.django-rest-framework.org/ #
#########################################
from django.conf.urls import url, include
from rest_framework import serializers, viewsets, routers
from backoffice.models import POI


# Serializers define the API representation.
class POISerializer(serializers.HyperlinkedModelSerializer):

    class Meta:
        model = POI
        fields = (
            'name',
            'lat',
            'lon',
            'desc',
            'tipologia',
            'categoria_prevalente',
            'indirizzo',
            'comune',
            'provincia',
            'numero_sedi',
        )


# ViewSets define the view behavior.
class POIViewSet(viewsets.ModelViewSet):
    queryset = POI.objects.all()
    serializer_class = POISerializer


# Routers provide a way of automatically determining the URL conf.
router = routers.DefaultRouter()
router.register(r'poi', POIViewSet)


# Wire up our API using automatic URL routing.
# Additionally, we include login URLs for the browsable API.
urlpatterns += [
    url(r'^api/', include(router.urls)),
    # url(r'^api-auth/', include('rest_framework.urls', namespace='rest_framework'))
]
